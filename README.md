Solucracy blog
=================

La méthode est en Creative Commons, tout le monde peut contribuer à son amélioration.
Ce blog est destiné à partager les expériences, à documenter la méthode et les outils que vous avez pu tester. 
Notre travail commun permettra de documenter une méthode efficace, de tester de nouvelles approches et de paver la route pour les suivants.

Pour améliorer la méthode :

1. Créer [un compte gitlab](https://gitlab.com)
2. Vous rendre sur la [forge dédiée à la méthode Solucracy](https://gitlab.com/solucracy/blog/tree/master/_posts/fr)
3. Cliquer sur EDI Web

![EDI web](img/ediweb.png)


4. Aller dans le dossier *_posts* puis dans le dossier *fr*

![Dossier post puis fr](img/post-fr.png)


5. Sélectionner l'article qui vous intéresse et le modifier en respectant le format [Markdown](https://fr.wikipedia.org/wiki/Markdown)

![commit](img/commit.png)

6. Cliquer sur commit
7. C'est tout. Un admin va valider votre contribution pour s'assurer que vous n'avez pas juste ajouté un lien pour vendre votre stock d'invendus de Viagra.

License
=================

Unless stated otherwise, all material in this blog is under [CC-BY-SA license](https://creativecommons.org/licenses/by-sa/4.0/) 
