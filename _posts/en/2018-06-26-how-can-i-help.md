---
layout: post
title: "How can I help?"
date:   2018-06-26
lang: en
ref: how-help
author: Yannick
categories: solucracy
---
There are multiple things you can do to help us move this site forward :

- Share <https://solucracy.com> with your friends
- Like and share the [facebook page][fb]
- Talk about it when you’re looking for a conversation topic
- Connect to the website and vote/comment on problems, add new problems, participate !
- Share the problems that impact you on social networks and start conversations about them
- If you think you can use this website for a personal or professional project, contact us, maybe we can make the site evolve to answer your needs more precisely.
- Share suggestions/ideas to improve the website with us, or let us know why you think it’s a bad idea, why you don’t want to participate, criticize the interface.
- Run around naked in the street with a megaphone, shouting “Solucracy.com !!!” if you’re more of an outdoor activist

[fb]: https://www.facebook.com/solucracy
