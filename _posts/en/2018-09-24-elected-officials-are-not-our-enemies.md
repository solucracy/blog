---
layout: post
title: "Elected officials are not our enemies"
date: 2018-09-24
lang: en
ref: elected-officials-are-not-our-enemies
author: Yannick
categories: solucracy
---

#### Introduction

To be quite honest, when I started working on the Solucracy project, my feelings towards elected officials were rather negative.

To me, the French administration was stored in my mind in a single suitcase, including the crooked politicians we are shown on TV and in newspapers all the time, the local elected officials and the entire public service.

When dealing with public services as a user, I always compared my experience to that which I could have had with a private company, whose sole objective is to make a profit.

I was approaching public services as a consumer.

Working on Solucracy forced me to try to understand how the tool could be useful to local administrations, what their problems are and what obstacles they face. I had the opportunity to talk with elected officials and employees from various municipalities, from several different parties, to attend public meetings, things that are within the reach of any citizen, all the time but that I had never done before.

My reasons for that were more or less valid: I have a job, I don't have the time, I pay taxes so I already contribute...etc....

And little by little, I was able to develop a different vision of the situation.

#### How I see the profession of elected official from the outside

The people who run in local elections are like you and me with a job, a life, a family... The only difference is that at some point in their lives, they decided to get involved in the management of their town, territory and simply to give a hand.

I do not deny that some people make this choice only out of personal interest, to have their land passed into a building zone, to stimulate their ego or to influence certain decisions. But it is far from being the most suitable career choice to become rich quickly ( see [salary scale for elected officials in France](https://www.collectivites-locales.gouv.fr/regime-indemnitaire-des-elus)) and the counterparts are quite harsh.

Many elected officials hold their positions part-time, depending on the size of the municipality.

And this position has many constraints. Imagine, for example, that you have a city to manage: you will have to make sure that the budget is respected, make continuous trade-offs to find out who is entitled to a grant and who is not, manage problems of incivility, the school system, the organization of various events, find funds for urban planning projects, navigate the countless administrative and legal procedures imposed by the system, manage a team of employees in place for several years when you are only here for 5 years, and the list goes on....

While collaborating with the opposition! People from various parties who will constantly question your decisions, only because the very nature of the system in which they operate requires them to disagree with you....

We have all met people in our lives whose main occupation is to disagree, and we know the energy it takes to make a collective decision when they are in the room.

But this is not necessarily the worst. The problem is that the population is against you!

How would you feel if, in your current work, in every decision you make, in every thing you do, hundreds of people judged that decision, criticizing every choice, explaining to you why you are wrong and why you have to justify yourself to them, exposing the situation until they relax?

I don't have a child, but I've often heard parents complain that their peers spend their time explaining to them how to raise their child. And it doesn't seem to be pleasant.

The pressure does not only come from the population but also from the department, the region, the state, which decide for you without knowing the context, impose standards and procedures that you must put in place without necessarily having the opportunity to negotiate or discuss.

Personally, I don't want this job, even if it was better paid. At least not as it is in the current system, I prefer to work for free in an association from time to time and avoid all these constraints.

#### Citizen participation

But it seems I'm not the only one who thinks that. All over France, alternative governance systems are being put in place, which make it possible to restore a bond of trust between elected representatives and citizens, and to turn public service users from consumers to contributors.

In addition to initiatives such as participatory budgets in [Metz](https://metz.fr/jeparticipe/#!/) or [Paris](https://budgetparticipatif.paris.fr/bp/), municipalism is gaining ground. It is no longer necessary to introduce the example of the [village of Saillans](https://reporterre.net/A-Saillans-les-habitants-reinventent-la-democratie), governed by its inhabitants.

The town hall of Archamps, the richest commune in France, has also incorporated participatory democracy into its governance model by using a system of [Project Action Groups](http://www.mairie-archamps.fr/conseil_municipal/democratie_participative) allowing citizens to work together on the issues that affect them.

[The collective of 36,000 municipalities](https://36000communes.org/) is also working to build a knowledge base to support citizens wishing to create a participatory list for the 2020 elections.

Increasingly, elected officials understand that the collective intelligence of the citizens of their territory allows them to accomplish more, quicker by integrating them into the thinking process once the need has been identified, leaving aside frustrating public meetings where all we do is present projects that have already been put together, decided without having asked the opinion of the people affected, where consultation is limited to "do you agree or disagree? Public consultations as currently practiced in the public sector run counter to all the recommendations of [change management professionals](https://www.commentcamarche.com/contents/147-conduite-du-changement) and can only lead to failure.

#### Conclusion

In conclusion, managing a city or village is a difficult activity, requiring a wide range of skills, and we are all responsible for it. The duty of a citizen is not only to vote, but also to participate in the development of his community and to support those who have volunteered to do so.

As a citizen, expect to be increasingly called upon! :-)

PS: If you wish to clarify or change this article, do not hesitate to use the [contact form](https://solucracy.com/homepage.php?contact=%22%22) or to contact us on the [Facebook page](https://www.facebook.com/Solucracy)
