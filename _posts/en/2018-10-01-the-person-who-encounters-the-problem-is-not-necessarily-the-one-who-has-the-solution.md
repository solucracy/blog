---
layout: post
title: "The person who encounters the problem is not necessarily the one who has the solution"
date: 2018-10-01
lang: en
ref: the-person-who-encounters-the-problem-is-not-necessarily-the-one-who-has-the-solution
author: Yannick
categories: solucracy
---
> "Hello, I have a problem, I'm looking for something to wedge my washing machine.
>
> Wedge your washing machine?
>
> Yes, the ground at home is not very level, it's an old building and when I start a program, it vibrates throughout the building, my neighbours complain about the noise. Are there any hard plastic blocks that I can use? I tried it with cardboard, but it eventually crashed. I have pieces of wood, but it is difficult to cut them lengthwise to reach exactly the right height.
>
> Have you tried to adjust the legs?
>
> Adjusting the legs?
>
> Yes, you can adjust the height of the legs of your machine by twisting and turning them as you wish."

This little conversation may sound silly, but many professionals in various fields have similar ones every day. In this case, the customer has a problem: his washing machine is wobbly. He tried to solve it in various ways, without success and would probably have continued if he had not had to explain his problem to the seller.

We regularly have an idea, invent the solution to our problem and try to implement it. If it is an application, we will think about a feature that allows us to accomplish something and harass the software publisher to implement it. "I absolutely need a red button here!"

All our conversations will tend to push our solution, we will convince other users to gain importance until we force the editor to add this button. And when he looks at it, he's going to ask us why we need this button, try to understand what we need to do to satisfy our need and maybe come to the conclusion that the easiest thing for everyone is just to add a green button elsewhere, or make an existing button more obvious.

Solucracy is based on the principle that it is more effective to start by defining the problem, the need, the lack and to understand the different ways in which it affects people. This allows several things:

- **Clarify the problem  **
  Often, the simple fact of having to explain our problem to someone else will force us to go around, simplify it, and transform it into something more understandable. It is not uncommon for the solution to appear on its own at that time.

- **Getting a different point of view**  
  These different problems can generate frustration, you are under the impression that you are hitting a wall and no matter how hard you look, you can't find a door. Putting it in the center so that everyone can observe it will show whether others have already solved it successfully, or whether they have heard of a solution that might be appropriate. It will also become more visible to experts in the field concerned.

- **Knowing that you're not alone**  
  You are probably not alone in experiencing this problem. Maybe others have exactly the same, or something very similar. If 50 or 500 people are also affected, the implementation of a solution can become more affordable. If an entire city is affected, then the construction of a building, a municipal by-law or a dedicated event becomes perfectly realistic.

- **Multiply the possible solutions**  
  The solution you can find on your own is part of a multitude of possible solutions, which will be more or less relevant. The intelligence, experience, expertise and creativity of the group, once focused on your needs, increases the chances of achieving a satisfactory result tenfold.

- **Get help**  
  Sometimes we are simply not in a position to solve our problem and sharing it will help us get help.


Identifying the problem and describing it correctly is a very important step in solving it. This makes it possible to establish precise specifications, so that others, companies, individuals and communities can participate in the development of a viable and satisfactory solution.
