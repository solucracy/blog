---
layout: post
title: "Purpose, mission and vision"
date:   2018-07-29
lang: en
ref: purpose-mission-vision
author: Yannick
categories: solucracy
---

At a time when the quest for meaning is a dominant activity for many of us, it is all the more important that companies are able to define, for themselves or for the people who compose them, the direction in which they are heading and the values that are specific to them.

That's why we've decided to try and describe what the Solucracy project represents as best we can.

## Purpose

"The purpose of an organization is to allow ordinary people to do extraordinary things. »

*Peter Drucker*

**Solucracy's purpose is to identify and prioritize the problems encountered by each community to facilitate the sharing of solutions.**

We keep hearing about the problems in our society, which will come back in a cyclical way, without ever feeling that nothing has progressed. Whether on television, during family meals or at work, we regularly come back to it without much progress. On the other hand, when we have a little time before us, there's no available list of areas for improvement, or of problems that have already been solved.

Where to start? Do the solutions already exist? Which one is better?

These are the questions Solucracy is trying to answer.

## Vision

"The idea must be born of the vision like the spark from the stone."

*Charles-Ferdinand Ramuz*

**Use current technologies to accelerate problem solving and focus collective intelligence to improve our society.

The fields of collective intelligence and citizen participation are evolving rapidly and more and more consultation techniques are emerging to enable a group of humans to make the best possible decisions. Most of the problems encountered in one place have already been satisfactorily solved elsewhere, there is no need to reinvent the wheel,we can adapt what already exists.

A single group of people can find a solution for a problem that impacts dozens of communities around the world. Until now, lack of visibility has been the only obstacle.

## Mission

"Everything on the earth has a purpose, every disease an herb to cure it, and every person a mission."

*Indian Wisdom*

**Provide tools for citizens to define the next challenge facing their local community and benefit from global support.

Thousands of people from all over the world come together and take up the challenges of living in society. It is time that each community could benefit from the progress of others. Solucracy will provide all the necessary tools to move from the problem to the implementation of the solution as easily as possible.

