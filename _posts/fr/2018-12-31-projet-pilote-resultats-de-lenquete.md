---
layout: post
title: "Projet pilote : Résultats de l'enquête"
date: 2018-12-31
lang: fr
ref: projet-pilote-resultats-de-lenquete
author: Yannick
categories: solucracy
---
Comme indiqué dans le précédent article, nous avons commencé à tester la méthode Solucracy sur le village de [Léaz (01200).](https://www.leaz.fr/-Francais-.html)

La collecte de données est maintenant terminée. Cet article se concentre sur les résultats et le prochain fera un retour d’expérience sur les outils utilisés, ce qui a fonctionné, n’a pas fonctionné, etc…

### Le lieu

Léaz est un village d’un peu plus de 700 habitants (380 boites aux lettres). Il est séparé en 3 hameaux : Grésin, Léaz et Longeray , distants chacun de plusieurs kilomètres et rejoints par une route départementale très fréquentée. L’école, la mairie et la bibliothèque sont toutes les trois sur Léaz. Chaque hameau possède un restaurant bar, bien que l’activité de celui de Longeray soit très limitée.

### Les résultats

Durant la semaine du 10 au 16 décembre, nous sommes donc passés chez les habitants pour les interroger sur la vie dans le village et avons obtenu 146 réponses. Étant donné que le site devra être adapté une fois la méthode finalisée, [les données brutes sont en attendant stockées ici ](https://docs.google.com/spreadsheets/d/1VTu_TG3I3jtJJ90rCnM2FyZEYdBqC3rS3956pdHAT5E/edit?usp=sharing).

2 types de formulaires ont été testés, un avec une approche positive et un avec une approche centrée plutôt sur les manques et les problèmes.

#### Qu’est-ce qui vous plaît dans le village ?

![Image](../img/word-art.jpeg)Le calme et la tranquillité d’une vie de village proche de la nature semblent être les atouts principaux de Léaz d’après les réponses. La proximité de la ville sans que son influence soit trop évidente et les rapports agréables avec les voisins sont remontés souvent.

Plusieurs personnes ont salué la création d’un Skate Park et l’extinction de l’éclairage public après minuit.

Les activités liées à l’école (spectacle de fin d’année, fête d’Halloween) ont été mentionnées le plus souvent. [La Léazienne (randonnée pédestre et VTT)](http://www.leazienne.com/page8.php), la pétanque, les activités organisées par l’association Faits et Gestes, le bal des pompiers, la gym, la belote ont été régulièrement mentionnées et en activités plus solitaires : le VTT, la pêche, les promenades ou randonnées et la chasse.

Sur l’image de gauche, les mots ressortis le plus souvent apparaissent en gros ( il apparait aussi que mes compétences en création de nuages de mots laissent à désirer ! :-) )

#### **Qu’est-ce qui peut être fait pour améliorer la qualité de vie dans le village ?**

Nous avons agrégé les résultats en catégories, même si les demandes n’étaient pas tout à fait similaires, pour obtenir une vue d’ensemble des sujets qui préoccupent les citoyens.


<table >
<tbody style="border: 1px solid black; border-collapse: collapse; text-align: center;">
<tr style="background-color: #5bc839; color: #fff;">
<td style="padding: 20px;">Catégorie</td>
<td style="padding: 20px;"><b>Léaz</b></td>
<td style="padding: 20px;"><b>Longeray</b></td>
<td style="padding: 20px;"><b>Grésin</b></td>
<td style="padding: 20px;"><b>Total</b></td>
</tr>
<tr>
<td>Petit commerce</td>
<td>32</td>
<td>14</td>
<td>16</td>
<td>62</td>
</tr>
<tr>
<td>Animation, cohésion sociale</td>
<td>12</td>
<td>17</td>
<td>15</td>
<td>44</td>
</tr>
<tr>
<td>Circulation, stationnement</td>
<td>5</td>
<td>24</td>
<td>14</td>
<td>43</td>
</tr>
<tr>
<td>Transports en commun</td>
<td>15</td>
<td>12</td>
<td>13</td>
<td>40</td>
</tr>
<tr>
<td>Activité,parc enfants</td>
<td>9</td>
<td>2</td>
<td>5</td>
<td>16</td>
</tr>
<tr>
<td>Propreté/déchets</td>
<td>3</td>
<td>5</td>
<td>6</td>
<td>14</td>
</tr>
<tr>
<td>Gestion de l'eau</td>
<td>1</td>
<td>7</td>
<td>2</td>
<td>10</td>
</tr>
<tr>
<td>Relier les 3 hameaux</td>
<td>2</td>
<td>0</td>
<td>7</td>
<td>9</td>
</tr>
<tr>
<td>Communication Mairie</td>
<td>0</td>
<td>5</td>
<td>3</td>
<td>8</td>
</tr>
<tr>
<td>Ecologie, environnement</td>
<td>0</td>
<td>5</td>
<td>3</td>
<td>8</td>
</tr>
<tr>
<td>Ecole/Crèche</td>
<td>5</td>
<td>0</td>
<td>2</td>
<td>7</td>
</tr>
<tr>
<td>Santé</td>
<td>1</td>
<td>2</td>
<td>2</td>
<td>5</td>
</tr>
</tbody>
</table>



1. **Un petit commerce :** Beaucoup aimeraient un dépôt de pain, une boulangerie, une petite épicerie, un dépanneur pour éviter d’avoir d’avoir à parcourir dix kilomètres en voiture pour aller chercher le pain ou faire des petites courses.
2. **Cohésion, lien social, animations : **Que ce soit pour les anciens ou les nouveaux habitants de Léaz, la cohésion sociale est importante. La majeure partie des habitants souhaitent plus d’activités, d’animation et rencontrer plus de gens tandis que ceux qui organisent ces activités regrettent qu’il n’y ait pas assez de participants. Ce thème sera très intéressant à explorer lors de l’atelier.
4. **Circulation, stationnement : **La vitesse excessive sur diverses routes de la commune et la dangerosité de la départementale sont souvent revenues. Des manques de trottoirs et de places de parking ont été signalés.
5. **Transports en commun : **La commune est, selon les retours, très mal desservie par les transports publics que ce soit en direction de Bellegarde ou du Pays de Gex/Genève. Pour les personnes ne possédant pas de voiture,, il est très difficile de sortir du village ou d’avoir des activités en dehors de la commune.
6. **Activités, parcs enfants : **Les habitants apprécient les installations existantes et souhaiteraient les améliorer un peu. Ils aimeraient également que les enfants aient accès à des activités ponctuelles (sorties ski, etc…)
7. **Propreté, déchets : **Certaines maisons, voitures sont laissées à l’abandon ici et là, certains usagers de la départementale ont tendance à jeter leurs poubelles un peu n’importe où
8. **Gestion de l’eau : **Divers commentaires ont été faits sur la gestion de l’eau (problèmes de pression, assainissement, fontaines publics, etc…)
9. **Relier les 3 hameaux : **La distance séparant les 3 hameaux rend difficile les interactions, la départementale étant trop dangereuse pour que les piétons puissent l’emprunter tranquillement. Il a donc été mentionné plusieurs fois d’aménager les bords du Rhône pour faciliter le trajet entre les hameaux.
10. **Communication avec la mairie : **Certains habitants pensent que la communication avec la mairie pourrait être améliorée tandis que d’autres en sont satisfaits. Il semblerait donc qu’il y ait une forme d’inégalité sur ce sujet, soit au niveau perception, soit au niveau des disponibilités.
11. **Ecologie,environnement :** Diverses questions ont été posées sur ce qui pourrait être fait pour le respect de l’environnement. Cette catégorie pourrait être rattachée à « propreté, déchets ».
12. **Santé : **Il n’y a pas de médecin traitant sur la commune de Léaz. Et apparemment, les médecins des alentours sont déjà surchargés.
13. **Divers : **Dans cette catégorie ont été rangés tous les besoins mentionnés moins de 5 fois, comme le raccordement à la fibre optique, le manque d’éclairage la nuit, l’envie de communiquer plus sur le Fort l’écluse, incivilités diverses,etc…

#### Nos impressions

Cette collecte d’informations nous a permis de rencontrer et de discuter avec une grande partie des habitants des 3 hameaux. Outre les données saisies dans le fichier, nous avons pu obtenir une vision d’ensemble de la perception du village par ses habitants.

La majorité aiment ce village pour son calme, sa tranquillité et sa proximité avec la ville sans qu’elle soit trop présente.  Il y a toutefois un décalage au niveau de ce qu’est une vie de village. Les nouveaux habitants venant de la ville apprécient les rapports de voisinage et les interactions. Par contre, pour les anciens du village, qui ont connu un tissu social plus développé ( tout le monde se  connait, les enfants du village sont élevés par tout le village, fêtes tous ensemble, etc…), la situation se détériore. Ils déplorent de ne plus connaître les gens qui vivent près de chez eux, de ne plus avoir de contact, que de moins en moins de gens viennent aux activités, qu’on n’aille plus chercher les anciens en maison de retraite lorsqu’il y a une fête de village.

Le point de rencontre privilégié, point névralgique des activités du village est apparemment l’école. Mais le fait que la commune soit séparée en 3 hameaux assez distants rend un peu plus difficile les contacts.

#### Prochaine étape

Tout cela pourra être exploré par les habitants lors de l’atelier participatif du 12 Janvier de 14h à 18h à la salle Maurice Raisin. Les résultats de cet atelier seront bien sûr partagés dans un prochain article !

Merci à Nicolas Pidoux et à Paloma Grasser pour leur aide dans cette opération de collecte d'information.
