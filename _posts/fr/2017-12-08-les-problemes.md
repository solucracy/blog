---
layout: post
title: "Les problèmes"
date: 2017-12-08
lang: fr
ref: les-problemes
author: Yannick
categories: solucracy
---
## Pourquoi enregistrer un problème ?

Le fait d’enregistrer un problème possède de nombreux avantages :

- Ca fait du bien !
- On a tous besoin de partager ce qu’on ressent, de laisser sortir nos frustrations. Cela aura le même effet que de le poser sur le papier. Plus besoin d’y penser.
- Le problème sera géographiquement localisé et associé à une catégorie.

Cela permettra aux autres utilisateurs de voir une liste des problèmes relatifs à une commune et de les filtrer par catégorie.

- Vous pouvez le partager sur les réseaux sociaux et faire voter la communauté.

Tous les membres de la communauté souffrant de ce problème pourront voter, ce qui permettra de le prioriser par rapport aux autres. Plus un problème aura de votes, plus il sera important de le résoudre.

Chaque vote possédant également une position géographique anonyme, l’impact réel du problème apparaitra ainsi sur la carte.
Par exemple, un carrefour dangereux peut impacter des gens qui habitent à 50 kilomètres mais qui passent tous les jours par là.

- Cela le rendra plus réel
- Ce que vous considérez comme un problème majeur ne représente peut-être qu’une gêne mineure pour un autre, ou peut-être que la plupart des gens sont passés par là, ont trouvé une alternative sans vraiment le régler et n’y pensent plus.

Par exemple, tous les matins, à cause d’un feu mal réglé, il y a un bouchon de 20 minutes à un endroit donné. X ne prend désormais plus cette route et a rallongé son trajet de 10 kilomètres pour l’éviter. Le problème le concerne toujours mais il n’y pense plus. Le régler lui faciliterait la vie mais cela ne fait plus partie de ses priorités.

- Nous pourrons réfléchir ensemble à une solution !Une fois que nous sommes conscients d’un problème, nous pouvons alors réfléchir ensemble à une solution.

Peut-être que quelqu’un qui n’est pas concerné possédait la solution depuis le début ?

Il y a parmi nous des ingénieurs, des artisans, des experts dans de multiples domaines, qui peuvent tous potentiellement nous aider.
Si vous avez la possibilité d’aider un groupe de personnes en postant juste un commentaire sur le site, allez-vous le faire ?

## Quels genres de problèmes puis-je entrer ?

Tous types de problèmes peuvent être signalés, c’est à vous de décider si cela en vaut la peine.

Un même type de problème qui impacte plusieurs endroits.

Est-ce que vous devez vous déplacer en fauteuil roulant et vous en avez marre de ne pouvoir accéder facilement à certains endroits ?

Les problèmes que plus personne ne voit.

Le but est d’améliorer les choses. Imaginons que vous ayez un caillou dans votre chaussure et que vous ayiez pris l’habitude de boiter pour souffrir moins. Grâce à Solucracy, nous trouverons quelqu’un pour retirer le caillou.

Vous êtes en voyage quelque part

En tant que visiteur, vous avez un point de vue différent. Vos critiques aideront peut-être à améliorer le tourisme, l’environnement.

En cas de sinistre

Si votre région a été balayée par des inondations ou une catastrophe naturelle, vous pourrez signaler tous les endroits ou les secours doivent intervenir, ou on manque d’eau potable

Tout ce qui peut améliorer la vie de la communauté

Un trou dans la route, une institution qui fonctionne mal, une entreprise qui pollue, un manque de quoi que ce soit où que ce soit. Si c’est un problème, ou si ça peut être amélioré, alors cela vaut la peine de l’entrer dans Solucracy.