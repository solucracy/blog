---
layout: post
title: "La personne qui rencontre le problème n'est pas forcément celle qui a la solution"
date: 2018-10-01
lang: fr
ref: la-personne-qui-rencontre-le-probleme-nest-pas-forcement-celle-qui-a-la-solution
author: Yannick
categories: solucracy
---

> "Bonjour,j'ai un problème, je cherche quelque chose pour caler ma machine à laver.
>
> Caler votre machine à laver ?
>
> Oui, le sol chez moi n'est pas très droit, c'est un vieux bâtiment et quand je lance un programme, ça vibre dans tout l'immeuble, mes voisins se plaignent du bruit. Existe-t-il des cales en plastique dur que je peux utiliser ? J'ai essayé avec du carton mais il a fini par s'écraser. J'ai des morceaux de bois, mais c'est difficile de les couper dans le sens de la longueur pour atteindre exactement la bonne hauteur.
>
> Avez-vous essayé de régler les pieds ?
>
> Régler les pieds ?
>
> Oui, vous pouvez ajuster la hauteur des pieds de votre machine en les vissant et les dévissant comme vous voulez."

Cette petite conversation peut paraître toute bête, mais de nombreux professionnels dans divers domaines en ont de similaires chaque jour. Dans ce cas précis, le client a un problème : Sa machine à laver est bancale. Il a tenté de le résoudre de diverses manières, sans succès et aurait probablement continué s'il n'avait eu à exposer son problème au vendeur.

Il nous arrive régulièrement d'avoir une idée, d'inventer la solution à notre problème et de tenter de la mettre en place. S'il s'agit d'une application, on va réfléchir à une fonctionnalité qui nous permet d'accomplir quelque chose et harceler l'éditeur du logiciel pour qu'il l'implémente. "Il me faut absolument un bouton rouge ici !"

Toutes nos conversations vont tendre à pousser notre solution, nous allons convaincre d'autres utilisateurs pour gagner du poids jusqu'à forcer l'éditeur à ajouter ce bouton. Et lorsqu'il va se pencher sur la question, il va nous demander pourquoi nous avons besoin de ce bouton, essayer de comprendre ce qu'il faut faire pour satisfaire notre besoin et peut-être parvenir à la conclusion que le plus simple pour tout le monde est juste d'ajouter un bouton vert ailleurs, ou de rendre plus évident un bouton existant.

Solucracy part du principe qu'il est plus efficace de commencer par définir le problème, le besoin, le manque et de comprendre les différentes manières dont il touche les gens. Cela permet plusieurs choses :

- **Clarifier le problème**  
  Souvent, le simple fait de devoir expliquer notre problème à quelqu'un d'autre va nous forcer à en faire le tour, le simplifier, et le transformer en quelque chose de plus compréhensible. Il n'est pas rare que la solution apparaisse d'elle-même à ce moment là.

- **Obtenir un point de vue différent**  
  Ces différents problèmes peuvent générer une frustration, on a l'impression qu'on se heurte à un mur et on a beau chercher, on ne trouve pas de porte. Le fait de le poser au milieu pour que tout le monde puisse l'observer va permettre de savoir si d'autres l'ont déjà résolu avec succès, ou s'ils ont entendu parler d'une solution qui pourrait convenir. Il deviendra également plus visible pour les experts du domaine concerné.

- **Savoir que l'on n'est pas tout seul**  
  Vous n'êtes probablement pas seul à rencontrer ce problème. Peut-être que d'autres ont exactement le même, ou quelque chose de très similaire. Si 50 ou 500 personnes sont touchées également, la mise en place d'une solution peut devenir plus abordable. Si une ville entière est touchée, alors la construction d'un bâtiment, un arrêté municipal ou un événement dédié devient parfaitement réaliste.

- **Multiplier les solutions possibles**  
  La solution à laquelle vous pouvez arriver tout seul fait partie d'un multitude de solutions possibles, qui seront plus ou moins pertinentes. L'intelligence, l'expérience, l'expertise et la créativité du groupe, une fois focalisées sur votre besoin décuple les chances de parvenir à un résultat satisfaisant.

- **Obtenir de l'aide**  
  Il arrive parfois qu'on ne soit tout simplement pas en position de résoudre notre problème et le fait de le partager permettra d'obtenir de l'aide.


Identifier le problème et le décrire correctement est une étape très importante de la résolution. Cela permet d'établir un cahier des charges précis, pour que d'autres, entreprises, particuliers, collectivités puissent participer à l'élaboration d'une solution viable et satisfaisante.
