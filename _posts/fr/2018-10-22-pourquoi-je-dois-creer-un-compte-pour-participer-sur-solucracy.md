---
layout: post
title: "Pourquoi je dois créer un compte pour participer sur Solucracy"
date: 2018-10-22
lang: fr
ref: pourquoi-je-dois-creer-un-compte-pour-participer-sur-solucracy
author: Yannick
categories: solucracy
---
Actuellement, pour ajouter un problème, une solution ou voter, il est nécessaire de créer un compte.

Plusieurs questions ont été posées sur ce sujet auxquelles je vais essayer de répondre ici.

##### Pourquoi suis-je obligé(e) de créer un compte ?

Il y a de multiples raisons à ça :

_Cela vous permet de conserver les droits d'auteur sur vos contributions. Solucracy, une fois que la structure aura été finalisée, sera considéré comme un bien commun, avec une méthode, une application et des données créées par les utilisateurs en open source. Si vous contribuez beaucoup, il est donc nécessaire que la communauté puisse reconnaitre votre participation à sa juste valeur.

_Les votes sur les différents problèmes sont géolocalisés et, bien que vous ne soyez aucunement obligés de renseigner une adresse exacte, la répartition des votes selon le lieu de résidence des personnes concernées est une information extrêmement importante pour comprendre qui sont véritablement les usagers d'un territoire.

_D'ici peu, vous aurez la possibilité de corriger/modifier le texte d'un problème si vous désirez le reformuler. Sans vous identifier, il est difficile de savoir qui en est l'auteur, et donc, qui peut le modifier.

_Si une personne désirait fausser le nombre de votes pour un problème, elle pourrait, s'il n'était pas nécessaire de créer un compte, juste voter plusieurs fois sur le même problème sans souci, en rechargeant la page plusieurs fois. Sans compte, il est difficile de garder une trace de qui a déjà voté ou non.

##### Pourquoi ne puis-je pas me connecter avec Facebook ou Google ?

Il y a 2 raisons à ça :

_La première est que je n'ai pas réussi à configurer cet interface correctement :-) . Vous me direz que plein de sites le font, que c'est un atout majeur et que ça mériterait que j'y passe un peu de temps mais je ne pense pas, à cause de la seconde raison.

_La seconde raison est que [Google et Facebook utilisent ces données pour gagner de l'argent](http://blog.liberetonordi.com/post/GAFAM). Ils pourront suivre votre parcours de site à site, etc... et établir un profil de navigation en fonction de ces informations collectées. Il vaut mieux donc, à mon avis, passer quelques secondes de plus à saisir vos pseudos et emails une fois pour créer un compte. Le site utilise encore Google Map et Google analytics et mon énergie va plutôt être dépensée pour sortir de ça plutôt que de s'enfoncer encore plus dans cette situation :-)

##### Pourquoi je ne peux pas rester connecté comme sur une application mobile ?

Ce cas là va dépendre de la manière dont vous utilisez votre navigateur. Si vous fermez votre navigateur, la plupart du temps, les données de navigation vont être effacées, donc il ne se souviendra plus que vous étiez connecté et vous devrez vous identifier à nouveau.

Si vous ne faites que mettre votre ordinateur en veille et que vous ne fermez ni votre navigateur, ni l'onglet du site, alors vous pourrez revenir sur la page sans souci, bien qu'il soit possible que le site vous déconnecte automatiquement dans un futur proche.

Lorsque la méthode Solucracy aura fait ses preuves, nous ferons développer une application mobile, qui devrait résoudre ces problèmes.
