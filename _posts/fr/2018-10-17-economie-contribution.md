---
layout: post
title: "L'économie de la contribution"
date: 2018-10-17
lang: fr
ref: economie-contribution
author: Yannick
categories: solucracy
---

Il y a 2 semaines, du 8 au 10 Octobre, une grande partie de l'équipe qui gravite autour de Solucracy a participé à une formation sur l'économie de la contribution avec [Lionel Lourdin](http://contribution.ch/Lionel-Lourdin/) à [Ideavox](http://ideavox.org/).

Cette formation nous a ouvert les yeux et permis, comme Lionel aime à le dire, d'élargir notre champ des possibles.N'étant absolument pas un expert dans le domaine, je vais tenter ici de restituer les éléments majeurs de cette formation mais je ne saurais que trop vous conseiller de garder un oeil ouvert sur les supports qui vont bientôt être mis à disposition en open source.
MàJ : Voici le [lien vers les documents de formation](https://contribution.ch/EC1/).

#### Les biens communs

L'économie de la contribution est basée sur un concept remis au goût du jour par [Elinor Ostrom](https://laviedesidees.fr/Elinor-Ostrom-Par-dela-la-tragedie-des-communs.html), lauréate du prix Nobel d'économie : les biens communs. Le bien commun représente un patrimoine matériel ou immatériel de la communauté humaine [selon wikipédia](https://fr.wikipedia.org/wiki/Bien_commun), qui est lui même un commun :-)

Par exemple, [l'eau potable de Grenoble](https://c-eau-region-grenoble.org/presentation-de-la-plateforme/fonctionnement/) est gérée comme un commun par une coopérative qui regroupe tous les usagers et intervenants. En Italie également, le [concept de bien commun est intégré dans la loi.](http://www.raison-publique.fr/article683.html)

[EDIT 23-10-2018] l'eau potable de Grenoble n'est pas gérée comme un commun mais par une [société publique locale](https://www.eauxdegrenoblealpes.fr/) qui appartient à la métropole, avec un comité des usagers. Voici par contre un article plus complet sur [l'assemblée des communs de Grenoble](http://semeoz.info/lassemblee-des-communs-de-grenoble-construire-la-ville-en-commun/) .

Les exemples ci-dessus sont basés sur des biens rivaux, dont la quantité est limitée, par opposition aux biens non rivaux, comme les œuvres intellectuelles ou la musique qui peuvent être copiées et distribuées sans que personne n'y perde.

Nous allons donc nous intéresser aux biens non rivaux, principalement les biens communs numérisés, pour la suite.

#### La culture de l'open source

Nous avons tous entendu parler de logiciels  open source. Ces logiciels, [soumis à des licences particulières](https://fr.wikipedia.org/wiki/Open_source), peuvent être utilisés, copiés, redistribués librement sous certaines conditions, généralement à condition de citer leur créateur et les personnes ayant participé à leur développement.

Ces logiciels sont des biens communs numérisés, gérés, améliorés, protégés par leurs usagers. Il y a ainsi beaucoup d'exemples de logiciels open source qui sont développés et financés par [des entreprises concurrentes.](https://www.zdnet.fr/actualites/microsoft-fait-une-entree-remarquee-a-la-linux-foundation-c-est-serieux-39844730.htm)

L'idée est donc que, plutôt que de réinventer la roue, on va utiliser un socle commun pour construire différents services. Par exemple, [Solucracy](https://solucracy.com/blog/remerciements/) a été uniquement construit grâce à des logiciels Open Source, donc 99,99999% du code a été écrit par une armée d'autres personnes, ce qui m'a permis de développer une grande partie du site tout seul.

L'industrie en général, se tourne également vers des modèles de [co-innovation](http://www.innovationpartagee.com/Blog/archives/co-innover-avec-un-concurrent-une-facette-meconnue-de-linnovation-ouverte/) plutôt que de dépenser des fortunes en R&D. Pour ne pas laisser les universités, les usagers, les clients et mêmes les concurrents participer à la résolution de leur problème ?

Si vous développez un logiciel destiné à améliorer le flux du trafic dans votre ville, pourquoi ne pas laisser les ingénieurs urbanistes du monde entier l'utiliser et participer à son évolution pour que vous puissiez obtenir un meilleur résultat ?

Mais le libre ne concerne pas uniquement le logiciel, la licence [CERN OHL](https://www.lemagit.fr/actualites/2240198460/Avec-sa-licence-OHL-le-Cern-cree-une-rampe-de-lancement-pour-lOpen-Hardware) permet de rendre libre les plans, documentation et méthodes pour des inventions par exemple.

#### Oui mais comment je mange ?

La première question, et la deuxième, et la troisième ont été : "C'est magnifique tout ça, mais il faut bien qu'on se nourrisse, qui va payer ?".

Il arrive souvent dans les domaines associatifs, militants, etc...constitués de passionnés, qu'on donne sans compter. On offre ses ressources, parce que justement, c'est pour le bien commun. Ca doit être fait et je me sens bien en le faisant.

Jusqu'au moment où on n'a plus d'argent pour vivre et on se retrouve à exercer un métier qui ne nous plaît pas forcément pour obtenir les ressources nécessaires à la poursuite de notre passion.

C'est là que se trouve la clé de ce système : En contribuant à ce bien commun, on développe une expertise, des connaissances, un savoir faire qui peuvent être organisés en services aux usagers.

Par exemple, [les windowfarms](https://www.consoglobe.com/cultiver-legumes-interieur-windowfarm-hydroponique-cg) sont un exemple parfait du bénéfice d'utiliser la communauté pour améliorer une invention ou un design. [Britta Riley](https://www.ted.com/talks/britta_riley_a_garden_in_my_apartment?utm_campaign=tedspread&utm_medium=referral&utm_source=tedcomshare), son inventeuse, a partagé les plans avec la communauté. Aussitôt, des centaines de personnes de par le monde en ont construit une, ont fait leurs propres expériences, amélioré le design,etc...

De son côté, Britta peut maintenant faire des formations, des conférences, fabriquer des windowfarms et les vendre juste en profitant du mécanisme des licences Open Source qui n'interdisent pas l'usage commercial.

#### Quel rapport avec Solucracy ?

Personnellement, cette formation m'a permis de dissoudre un gros paquet de tensions intérieures. La vision pour Solucracy ne cadrait pas du tout avec le système actuel. Pourquoi se protéger, empêcher les gens de s'approprier les idées et expériences mises sur le site alors que nous essayons tous d'accomplir la même chose ?

Il n'y a maintenant plus aucun obstacle, que ce soit au niveau des valeurs, ou des ressources pour vivre de notre passion. Attendez-vous donc à plein de bonnes surprises dans les semaines qui viennent !
