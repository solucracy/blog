---
layout: post
title: "Pourquoi avez-vous besoin de mes coordonnées ?"
date: 2018-07-26
lang: fr
ref: besoin-coordonnees
author: Yannick
categories: solucracy
---
L’objectif de Solucracy est de définir combien de personnes sont concernées par des problèmes sur une certaine zone géographique.
Lorsque vous voterez pour un problème, cela ajoutera un point sur la carte du problème.

Ainsi, si ce problème est relatif à votre commune, vous pourrez savoir combien de personnes sur votre commune sont concernées.
Cela permettra aux administrateurs de votre commune d’obtenir une liste des problèmes à régler sur leur juridiction.

> IMPORTANT: Les autres utilisateurs ne pourront pas connaitre vos coordonnées et nous ne les transmettront à personne.
