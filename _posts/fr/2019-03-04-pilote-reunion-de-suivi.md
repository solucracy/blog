---
layout: post
title: "Projet pilote : Réunion de suivi des projets"
date: 2019-03-04
lang: fr
ref: pilote-reunion-de-suivi
author: Yannick
categories: solucracy
---


Vendredi soir 1er Mars a eu lieu la réunion de suivi des projets qui ont émergé à Léaz lors du forum ouvert. 12 personnes étaient présentes (les 2 animateurs de Solucracy compris).
Voici donc le compte rendu de cette réunion ! :-)

## Petit tour de parole.

## Constitution de l’agenda :

Une fois les éléments triés, un agenda en 5 points pour la réunion était prêt.

1. Point de vue Solucracy
2. Quels sont les projets représentés dans cette réunion, ou en sont-ils, combien y en a-t-il et y en a-t-il de nouveaux ?
3. Recrutement/astuces pour fédérer les personnes autour d’un projet
4. Fonctionnement interne aux projets : Comment s’organiser autour d’un projet, quels sont les besoins/obstacles d’un porteur, comment bien communiquer au sein d’un projet ?
5. Fonctionnement externe aux projets : Comment garder du lien entre les différents projets et collaborer ?


## 1.Point de vue Solucracy 

Le but de ce projet et tout ce qu’on fait ici est de vous donner les outils, la méthode et l’expérience pour pouvoir lancer l’année prochaine ou dans 6 mois, un autre projet Solucracy.
De créer une équipe de 5-10 personnes pour faire du porte à porte, agréger les résultats, tenir les gens au courant, organiser l’atelier participatif.
Si l’aventure vous intéresse, vous avez l’opportunité de lancer une nouvelle tradition dans votre village, un événement par an : Atelier participatif + repas dansant et générer de nouveaux projets ou avoir des gens qui s’inscrivent aux projets existants.
Tout ça pour créer une dynamique. Vous êtes un peu les jardiniers du village. Vous avez décidé par votre engagement à prendre soin de Léaz et on va essayer de vous fournir tout ce qu'il faut pour que vous puissiez arroser et faire pousser la convivialité, nourrir la communication et récolter et partager les fruits de votre travail.

## 2.Etat des lieux des projets en cours

    • Collectif citoyen

Le collectif citoyen se donne pour mission de faire avancer les projets de manière homogène, et transmettre les informations pour qu’ils puissent s’associer et contribuer, ouvrir les portes.
Pas de nécessité pour une hiérarchie, transmettre les valeurs en commun, collecter les besoins d’intérêt général et faire l’interface avec les élus.

    • Enfance

Micro-crèche, activité pour les enfants. Un SMS a été envoyé aux personnes inscrites sans obtenir de réponse. Il n’y a donc plus d’énergie dans ce projet, s’il doit être relancé, il le sera, mais est mis de côté pour le moment.

    • Convivialité/partage

Réunion à l’auberge de Léaz un mois auparavant, décision de lancer un pique nique entre les 3 villages plutôt en septembre.
Système d’échange local : Utiliser une partie des panneaux d’affichage de la mairie pour poster des messages
Jardins collectifs : Une parcelle a été léguée à la mairie spécifiquement pour ça. Après discussion, le projet semblait compliqué, comment s’organiser, comment organiser les tâches. Il y a apparemment un jardin partagé qui s’est monté à St Jean de Gonville avec l’aide des Colibris, possibilité de lancer un partage d’expérience.
Apparemment, dans le journal le Grêlon, un appel a été lancé pour s’occuper de ce projet.
Le projet est au point mort pour l’instant. L’étape suivante reste à définir.

    • Fourrière animale

Christiane avance seule sur ce projet. Très contente du progrès, les rendez-vous avec les différents organismes ont été pris, il ne reste plus qu’à trouver un coin pour mettre les boxes.
Elle se débrouille et n’a pas besoin d’aide.

## 3. Recrutement/astuces pour fédérer les personnes autour d’un projet	

Suite aux différentes discussions, il est décidé que ce point est essentiel et après une séance de réflexion individuelle, la mise en commun est si riche qu’il est décidé de concentrer les efforts sur ce sujet et de mettre de côté les 2 derniers points pour plus tard.
Le collectif citoyen est mis en avant pour devenir le catalyseur des énergies et ressources pour chaque projet.
Plusieurs thèmes/idées ressortent donc de la réflexion :

Le texte en italique représente le texte écrit sur les post-its
 - Recueil d’idées, de besoins : *Boites à suggestions, information en libre expression dans le village, grand tableau blanc dans chaque village*
 - Information directe : *information dans les boites aux lettres, aller vers les gens pour les inviter, enquêtes d’opinion, porte à porte, faire du porte à porte pour expliquer la démarche des collectifs*
 - Outils numériques : *Créer un site web de plus lié à la mairie, mailing aux citoyens pour faire connaître le collectif citoyen, rubrique sur Leaz.fr « boite à idées » collectif citoyen, créer une page Facebook*
 - Qui ? Comment ? Pourquoi ? *Expliquer les pourquois (raison d’être, vision, mission, valeurs), informer largement de l’existence du collectif citoyen. Répéter plusieurs fois le message. Expliquer aux gens la notion de collectif et l’intérêt de celui-ci par le biais du grêlon, mail , courrier, bouche à oreilles.Présenter les résultats dans d’autres communes ayant un collectif citoyen, publier régulièrement les avancées des projets.Un premier exemple d’action collective qui fonctionne.*
 - Projet fédérateurs : *Des projets concrets, commune Zéro déchets, fédérez autour d’un projet.*
 - Événements informatifs éducatifs : *Inviter quelqu’un qui peut partager une expérience vécue dans ce domaine + présentation du projet, organiser une conférence débat,conférences sur des thèmes clés, des conférences et des films pour donner envie*
 - Ressources locales : *Lien fort avec la commune-partenariat → donner une idée de sérieux, faire une liste de candidature à la mairie regroupant toute la population cf Saillans, Drôme, Biblio, école, enfants, impliquer les jeunes, qu’ils soient portes parole, passer par l’ensemble des associations du village pour informer et fédérer*
 - Médias : *Rubrique dans le grêlon « news du collectif citoyen », lettre d’info du maire, Radio Sorgia FM, journaux/tribune*
 - Outils opérationnels : *Un projet Solucracy par an, rotation sur les tâches, définir un calendrier récurrent de réunions, forum ouvert, réunions publiques régulières.*
 - Événements festifs : *Couper Internet, la télé et les routes pendant une journée,  Plaisir ! Donner l’envie de faire quelque chose ensemble, de mener à bien des projets. Réunir les personnes qui peuvent être partantes de façon attractive pour démarrer, pique nique, organiser une journée d’accueil pour les nouveaux habitants afin de leur présenter ce qui se fait dans le village ( associations, collectif citoyen), mettre en place le pique nique des 3 villages, faire un stand pour le collectif aux fêtes (lotos,etc..), le pique nique est une bonne occasion pour présenter les projets aux habitants, s’inviter chez les gens pour le café, organiser une soirée promo, jeux collectifs, méthode espagnole ( de 19h à 20h, les villageois sortent tous de chez eux et boivent l’apéro chez les uns et les autres), réunions de quartier.*

## Tour de clôture.

## Apéro/buffet canadien !
