---
layout: post
title: "Quelle plateforme numérique pour soutenir Solucracy ?"
date: 2019-03-05
lang: fr
ref: plateforme-numerique-solucracy
author: Yannick
categories: solucracy
---

### Compte rendu de l'atelier du 2 Mars 2019

Samedi 2 Mars, nous nous sommes donc réunis chez [Lionel Lourdin](http://contribution.ch/Lionel-Lourdin/) à [Ideavox](http://ideavox.org/) pour définir ensemble l'outil idéal pour accompagner la méthode Solucracy.

Cette rencontre a avant tout été humainement très riche car peuplée de gens magnifiques travaillant sur des projets fantastiques !
Les échanges sur les besoins et les attentes des personnes présentes m'ont encouragé à penser qu'il est plus utile d'avoir une approche modulaire.
La prochaine étape est donc une documentation du champ fonctionnel de chaque module.
Mais voici 

Voici les contributeurs de cet atelier :

[David Dräyer](https://ateliersinstantz.net/david-drayer/)

Johan Morant

[Lionel Lourdin](http://contribution.ch/Lionel-Lourdin/)

[Nicolas Pidoux](https://www.linkedin.com/in/nicolas-pidoux-535660aa/)

[Nyasha Duri](https://www.linkedin.com/in/nyashaduri/)

Rachid El Abbouti

[Robert Stitelmann](https://youtu.be/J6KLiIIHN8I)

Sabrina Vuillemenot

[Stephan Krajcik](https://www.art-link.net/fran%C3%A7ais/profil/)

[Yannick Laignel](https://www.linkedin.com/in/yannicklaignel/)

[Yvan Schallenberger](https://ateliersinstantz.net/yvan-schallenberger/)

#### Déroulement de l'atelier

Après un tour d'ouverture et une présentation du lieu par Lionel, j'ai pu raconter l'histoire du projet Solucracy, sans omettre aucun détail, au risque d'endormir tout le monde :-) . 
Pendant la présentation, les participants avaient pour mission de noter sur des post-its les besoins qu'ils identifiaient, pour eux ou par rapport au projet.

Suite à cet exercice, il y a eu un moment de mise en commun des besoins, en tentant de les répartir selon les différentes étapes de la méthode ce qui a donné ceci (permettant au passage de partager avec vous mes talents en photographie...) :

![Image](../img/besoins.jpg)

Vu que je suis nul en photo, me voilà donc forcé de tout ré-écrire....Nous avions donc 5 cases représentant les phases chronologiques de la méthode + 2 bonus :

+ Collecte d'infos

Post its : Champs à remplir, comment remonter les besoins, quels supports ? (papier), liste d'éléments descriptifs des problèmes identifiés (géolocalisation, importance, temporalité),collecter des infos pour divers organismes ( association, pêche, cycliste,etc..), collecte d'infos plus détaillées en temps réel, avec modérateur.

+ Rapports

Post-its : 

A droite :Priorisation, marchés prédictifs, synthèses (formules,coefficients)
En bas à gauche : un projet, un territoire, une histoire, les habitants au coeur du projet, créer une communauté avant tout, créer un comité de pilotage, comment fédérer ?

+ Ateliers Participatifs

Post-its : 
A gauche : Moyen d'éduquer, informer, sensibilisation, financement, comment faire pour que collectivités et citoyens s'engagent.
En bas : jugement majoritaire, système de vote.
A droite : Tristan (Saillans), démocratie participative, rêve du dragon, initier et fédérer.

+ Accompagnement projets

Post-its : Accompagner jusqu'à l'autonomie, pérennité, créer du lien entre les acteurs, structurer des apports, des "jardins partagés" au service de l'input individuel et du groupe, soutien des collectivités, allocation de fonds aux projets, comment l'appliquer ?, Implémentation des projets, méthode pour communiquer efficacement, gouvernance partagée avancée, outils pour débattre, mettre en rapport des gens qui ont un besoin et ceux qui ont une solution

+ Mise en réseau des projets

Post-its : Partage des bonnes pratiques identifiées, adaptabilité aux thématiques larges, mise en réseau des "projets, événements" Solucracy Carto, transiscope, construction de réseaux, Jardin du Nous.

+ Autres

Post-its : Log in commun, création d'un compte utilisateur, grosse soirée pour info, facilitation de l'interface, géolocalisation, interopérabilité, plate forme évolutive, pour permettre de designer l'architecture de la future plateforme, design techno centralisé ou distribué ?, changer le langage PHP en node.js, méthode évolutive, modules, besoin d'imager la méthodologie Solucracy pour comprendre la systémique ( besoin de schéma logique).

+ Valeurs,vision

Post-its : Légitimité rassurante, l'entraide, être acteur et responsable, Raison d'être Solucracy ? Transformer le citoyen en contributeur, fournir moyen facile méthode, le citoyen décideur, le partage de connaissance

Et parce qu'on aime bien le format du forum ouvert, nous avons ensuite établi l'agenda de l'atelier :

![Image](../img/agenda.jpg)

Voici les restitutions des différentes discussions :

#### Session 1

**Centralisé ou distribué.**

La question était de savoir si Solucracy était un système central à la Facebook stockant toutes les données ou un système distribué offrant la possibilité aux utilisateurs de l'utiliser en l'installant chez eux.
Nous avons opté pour le mode mère -> enfant, un module parent qui agrège les infos produites par des modules locaux et les centralise pour offrir une vue d'ensemble.
Le mode peer to peer était également intéressant mais pas technologiquement prêt, ou trop compliqué à mettre en oeuvre.
Participant à cette conversation, elle a fait émerger plusieurs peurs chez moi :-)  : 
_Je n'ai pas envie de gérer une compagnie de logiciels et donc la responsabilité d'avoir à maintenir le module parent m'a un peu fait reculer. J'aurais préféré le mode peer to peer avec un réseau qui survit sans moi.
_La question du modèle économique qui est problématique pour moi mais essentielle à la survie du projet, ici le principe serait d'utiliser la vente de services liés à la centralisation de l'information.

**Comment mettre en relation problèmes et solutions/Prioriser : Marché prédictif ou autre ?**

Je vais retranscrire ici les notes prises par Yvan, parce qu'elles sont finalement plus efficaces que toute phrase que je pourrais faire :

Comment mettre en relation problème et solution ?

Piste

Qui met en lien les problèmes et les solutions

Prioriser les problèmes ?

Prioriser les solutions ?

Comment choisir les projet citoyen gagnant ?

Utiliser

Enjeux strategique

Marché predictif

Une réponse à un changement d’echelle

Justifiable scoentifiquement

Utiliser les outils de Émile Servan Schreiber

Finance,est participatif

Implication prédictif payant ?

Bien investir son temps

Savoir qu’il ne sont pas seul

Savoir que nous pouvons être capable d’intelligence collective performante

Cadrer les bonnes questions

Énoncer la question

Affiner la question

Méthode d’IC

Facteur...lieu, date, module thématique

Enjeux moyen

Jugement majoritaire

Enjeu terrain

GPC

Place du marché

Problème solution

Travaux.com

#### Session 2

**La collecte des infos et le lien avec le numérique**

Cette discussion a soulevé le problème lié au fait de réunir des informations à numériser ensuite sans pouvoir les associer à des utilisateurs existants.

Nous avons conclu que les données collectées pouvaient rester anonymes lorsqu'elles avaient été collectées par un porte à porte mais que la création d'un formulaire web nécessitait une identification pour éviter les trolls.
La solution à ça a donc été de définir que la seule manière de pouvoir créer un compte sur le site est d'avoir une conversation avec un médiateur Solucracy lors de la phase de collecte, des forums ouverts, etc... pour éviter la création de multiples comptes fantômes. Une sorte de système de validation en live.
Technologiquement, il reste à voir comment gérer ça :-)

Nous avons également parlé de la collecte d'infos elle-même et du fait que se limiter aux 4 questions généralistes allait diminuer l'intérêt de la méthode.
De la résistance de mon côté à ce niveau là également, car, pour moi, l'approche généraliste des questions permet vraiment de réaliser une étude de marché à 360 degrés et d'ouvrir les perspectives sur les besoins du territoire.
Si les questions sont libres, et peuvent être limitées à un sujet particulier, alors on ne fait plus vraiment remonter les besoins, on fait juste un sondage.
L'argument de Robert et de Lionel était que différentes professions pourraient s'approprier la méthode si le choix des questions reste ouvert.
Mais ma peur est de se retrouver avec une récupération comme on a l'habitude de voir en politique : 
*Vous préférez manger la merde que j'ai dans la main gauche ou celle que j'ai dans la main droite ?*

Pour faire taire ma peur, l'idée qui en est sorti est de définir une "charte" Solucracy pour les questions posées en précisant qu'elles doivent répondre à certains critères qui restent à définir pour respecter l'esprit de la démarche et éviter les questions orientées.

**Gouvernance,vote, prise de décision/Pertinence et partage autres méthodes**

 Gouvernance, vote, prise de decision

Je connais pas

Arts de prendre des decisions

Structurer l’organisation

Chef ou pas ?

Horizontal ou vertical

Système de vote

Ne pas se sentir lésé

Autres approches que celle posée par solucracy

Sociocratie

Rêve du dragon

Module

Sociocratie

GPC

ESC

Rêve du dragon

Premier pas

Holacratie

Jugement majoritaire

Vote

Différent scrutin

Technologie

Block-chain

Impact

Holochain

Open source

Distribu

Utilisation en locale

Utilisation sur des enjeux strategiques

Configurer les questions


#### Session 3

**Quelle technologie choisir ?**

Cette discussion a une nouvelle fois fait émerger le fait que je n'ai pas l'intention de devenir une boite de gestion de logiciel, et qu'il vaille mieux s'adosser à un outil existant.
Ou créer un pack mélangé d'humains, de techno et de méthodologie pour faire évoluer des modules d'outils
Ma peur est donc de me retrouver à gérer un truc trop lourd ou à devoir mettre trop de développement pour faire évoluer les outils.
Robert a très justement souligné qu'il ne faut pas oublier que l'intention est de booster la résolution de problèmes pour les citoyens.

**Solucracy pour les nuls / Logo et couleur interface**

Cet atelier est parti du constat que la mission de Solucracy est un peu floue et son interface loin d'être intuitif et parfait :-) . 
Les idées ont donc été :

_Une vision cartographiée des projets Solucracy

_Un logo qui pète

_Des couleurs neutres, plus de couleurs différentes plutôt qu'uniquement vert

_S'inspirer de ce qui existe déjà pour un interface qui utilise des trucs 
connus

_Répondre aux questions : pourquoi les gens viennent ? Qu'est-ce que c'est ? Qu'est-ce qui se passe près de chez moi ?

_Travailler la sémantique et le langage utilisé

_Insuffler un paradigme d'entraide, plus accueillant/ludique



