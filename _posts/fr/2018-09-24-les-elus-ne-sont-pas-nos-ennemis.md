---
layout: post
title: "Les élus ne sont pas nos ennemis"
date: 2018-09-24
lang: fr
ref: les-elus-ne-sont-pas-nos-ennemis
author: Yannick
categories: solucracy
---

### Introduction

Pour être tout à fait honnête, lorsque j'ai commencé à travailler sur le projet Solucracy, mes sentiments envers les administrations des collectivités étaient plutôt négatifs.

Pour moi, l'administration française était rangée dans ma tête dans une unique valise, comprenant les hommes politiques véreux qu'on nous montre à la télé et dans les journaux à longueur de temps, les élus locaux et la totalité du service public.

Lorsque j'avais affaire au service public en tant qu'usager, je comparais toujours mon expérience à celle que j'aurais pu avoir au contact d'une entreprise privée, dont l'unique objectif est de faire du profit.

J'abordais le service public en tant que consommateur.

Le fait de travailler sur Solucracy m'a forcé à essayer de comprendre comment l'outil pourrait être utile aux administrations locales, quelles sont leurs problématiques et quels obstacles elles rencontrent. J'ai eu l'opportunité de discuter avec les élus et employés de diverses communes, de plusieurs partis différents, d'assister à des réunions publiques, choses qui sont à la portée de n'importe quel citoyen, tout le temps mais que je n'avais jamais fait.

Mes excuses pour ça étaient relativement valides : J'ai un travail, je n'ai pas le temps, je paie des impôts donc je contribue déjà...etc...

Et petit à petit, j'ai pu développer une vision différente de la situation.

### Comment je vois le métier d'élu de l'extérieur

Les personnes qui se présentent aux élections locales sont comme vous et moi avec un métier, une vie, une famille... La seule différence est qu'à un moment de leur vie, elles ont décidé de s'impliquer dans la gestion de leur commune, territoire et tout simplement de filer un coup de main.

Je ne nie pas que certaines personnes font ce choix uniquement par intérêt personnel, pour faire passer leur terrain en zone constructible, stimuler leur égo ou influencer certaines décisions. Mais c'est loin d'être le choix de carrière le plus adapté pour devenir riche rapidement ( voir [barême de rémunération des élus](https://www.collectivites-locales.gouv.fr/regime-indemnitaire-des-elus) ) et les contreparties sont assez rudes.

Beaucoup d'élus occupent leur poste à mi-temps, suivant la taille de la commune.

Et ce poste présente beaucoup de contraintes. Imaginez par exemple que vous ayez une ville à gérer : vous devrez vous assurer que le budget est respecté, faire de continuels arbitrages pour savoir qui a droit à une subvention et qui n'y a pas droit, gérer les problèmes d'incivilité, le système scolaire, l'organisation de divers événements, trouver les fonds pour les projets d'urbanisme, naviguer les innombrables procédures administratives et légales imposées par le système, gérer une équipe d'employés en poste depuis plusieurs années alors que vous n'êtes là que pour 5 ans, et la liste continue....

Tout en collaborant avec l'opposition ! Des gens de divers partis qui vont remettre en question perpétuellement vos décisions, uniquement parce que la nature même du système dans lequel ils évoluent exige qu'ils ne soient pas d'accord avec vous...

On a tous croisé dans notre vie des personnes dont la principale occupation est de ne pas être d'accord, et on sait l'énergie qu'il faut dépenser pour parvenir à prendre une décision collective quand elles sont dans la pièce.

Mais là n'est pas forcément le pire. Le problème est que la population est contre vous !

Comment vous sentiriez-vous si, dans votre travail actuel, à chaque décision que vous prenez, chaque chose que vous faites, des centaines de personnes jugeaient cette décision, critiquant le moindre choix, vous expliquant pourquoi vous avez tort et que vous deviez vous justifier auprès d'elles, exposer la situation jusqu'à ce qu'elles se détendent ?

Je n'ai pas d'enfant mais j'ai souvent entendu des parents se plaindre que leurs pairs passent leur temps à leur expliquer comment élever leur enfant. Et ça n'a pas l'air d'être agréable.

La pression ne vient pas uniquement de la population mais aussi du département, de la région, de l'état, qui décident pour vous sans connaître le contexte, vous imposent des normes, des procédures que vous devez mettre en place sans forcément avoir la possibilité de négocier ou discuter.

Personnellement, je ne veux pas de ce travail, même si c'était mieux payé. Du moins pas tel qu'il est dans le système actuel, je préfère œuvrer gratuitement dans une association ponctuellement et échapper à toutes ces contraintes.

### La participation citoyenne

Mais il semblerait que je ne sois pas le seul à penser ça. Partout en France, des systèmes de gouvernance alternatifs se mettent en place, qui permettent de restaurer un lien de confiance entre les élus et les citoyens, et de refaire passer les usagers du service public de consommateurs à contributeurs.

Outre les initiatives comme les budgets participatifs à [Metz](https://metz.fr/jeparticipe/#!/) ou à [Paris](https://budgetparticipatif.paris.fr/bp/), le municipalisme gagne du terrain. Il n'est plus besoin de présenter l'exemple du [village de Saillans](https://reporterre.net/A-Saillans-les-habitants-reinventent-la-democratie), gouverné par ses habitants.

La mairie d'Archamps, commune la plus riche de France, a également incorporé la démocratie participative à son modèle de gouvernance en utilisant un système de [Groupes Actions Projets](http://www.mairie-archamps.fr/conseil_municipal/democratie_participative) permettant aux citoyens de travailler ensemble sur les problématiques qui les touchent.

[Le collectif 36000 communes](https://36000communes.org/) travaille également à constituer une base de connaissances destinée à accompagner les citoyens souhaitant créer une liste participative pour les élections de 2020.

De plus en plus, les élus comprennent que l'intelligence collective des citoyens de leur territoire leur permet d'accomplir plus, plus rapidement en les intégrant à la réflexion une fois le besoin identifié, laissant de côté les réunions publiques frustrantes où l'on ne fait que présenter des projets déjà ficelés, décidés sans avoir demandé l'avis des personnes touchées, où la consultation se limite à "vous êtes d'accord ou pas ?". Les consultations publiques telles qu'elles sont pratiquées dans le secteur public actuellement vont à l'encontre de toutes les recommandations des professionnels de la [conduite du changement](https://www.commentcamarche.com/contents/147-conduite-du-changement)  et ne peuvent que conduire à l'échec.

### Conclusion

Pour conclure, la gestion d'une ville ou d'un village est une activité difficile, qui nécessite un panel de compétences très varié, et nous en sommes tous responsables. Le devoir de citoyen ne se limite pas à voter mais il consiste également à participer à l'évolution de sa communauté et à soutenir les personnes qui se sont portées volontaires pour accomplir cette tâche.

En tant que citoyen, attendez-vous à être de plus en plus sollicités ! :-)

PS : Si vous désirez apporter des précisions ou faire évoluer cet article, n'hésitez pas à utiliser le [formulaire de contact](https://solucracy.com/homepage.php?contact=%22%22) ou à nous contacter sur la [page Facebook](https://www.facebook.com/Solucracy)
