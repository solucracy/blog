---
layout: post
title: "Les besoins des acteurs de la transition"
date: 2019-03-21
lang: fr
ref: les-besoins-des-acteurs-de-la-transition
author: Yannick
categories: solucracy
---

Du 9 au 16 Mars 2019 a eu lieu [la biennale des villes en transition à Grenoble](http://villesentransition.grenoble.fr/).
Cet événement a été l'occasion pour nombre d'acteurs de la transition de se réunir et de mettre en commun leurs efforts. Le programme a été tellement riche que je ne vais même pas essayer de le détailler ici. Vous n'aviez qu'à y être ! :-) 

Par contre, en mode off, j'en ai profité pour tester si la méthode Solucracy pouvait s'appliquer pour un contexte autre qu'un territoire et ait donc posé ces 4 questions :

*Au sein du mouvement de la transition,*

*Qu'est-ce qui vous plait ?*
*A quelles activités aimez-vous participer ?*
*Quelles activités pourrait-il y avoir de plus ?*
*Qu'est-ce qu'il pourrait y avoir de plus pour améliorer la qualité de la vie ?*

Au niveau du public choisi, j'ai posé les questions aux gens qui étaient là et qui étaient disponibles, donc pas de procédure ultra sophistiquée pour sélectionner une démographie représentative (prends ça IFOP ! :-) ) et j'ai également lancé un petit questionnaire en ligne ensuite auquel 5 personnes ont répondu.

En tout, nous avons donc 28 réponses, ça parait peu mais ça donne déjà des résultats très intéressants.

Les données brutes sont [ici](https://gitlab.com/solucracy/blog/tree/master/static/fr/enquete-transition.ods)

#### Qu'est-ce qui vous plaît dans ce mouvement ?

J'aime bien les nuages de mots pour représenter les résultats donc voici un petite florilège qui parle de lui même :-) :

![Image](../img/transition.png)

#### Et les autres questions...

Pour savoir comment représenter tout ça, j'ai tourné en rond un moment dans ma tête.

C'est plutôt une image qui apparait en lisant les réponses, donc tout ce que je peux vous donner est une interprétation. Je vous encourage à aller voir les données brutes :-) .

En gros, ce mouvement est profondément humain. Bien que beaucoup de personnes interrogées vivent avec peu de moyens, la question financière n'est ressortie que 5 fois et uniquement comme moyen, pas comme une fin en soi.

Il existe un besoin pour plus de lien, plus de mise en commun, de mise en réseau. Beaucoup sont conscients de l'énergie qui pourrait être économisée en mutualisant les outils, les efforts, etc...

Le fait de prendre soin de l'individu revient souvent également, un besoin d'accompagnement, de soutien psychologique, d'écoute, de rencontre.

Il semblerait qu'il faille encore plus de prise en compte du ressenti, de transition intérieure.

Voilà, je peine un peu à condenser la richesse des réponses et en fait, j'ai pas envie de vous donner une excuse de ne pas aller les lire :-) .
