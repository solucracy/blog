---
layout: post
title: "Besoins pour l'application"
date: 2019-03-21
lang: fr
ref: besoins-pour-l-application
author: Yannick
categories: solucracy
---

L'objectif de cet article est de définir clairement les besoins d'une application destinée à soutenir les gens qui souhaitent déployer la méthode Solucracy.

[Communecter](https://www.communecter.org/) semble être le logiciel libre le plus adapté pour répondre à ces besoins.

Pour définir ces besoins, le plus simple est de décomposer le cahier des charges en modules. La raison a ça est que chaque module pourra être récupéré et adapté pour d'autres activités, d'autre méthodes, d'autres personnes.

Ca va probablement évoluer en fonction de vos commentaires également.

Dans les descriptions qui suivent, j'ai délibérément ignoré les fonctionnalités de base type création de compte, gestion des droits, notifications, etc... qui sont déjà présentes dans Communecter.

**1. Réunir les besoins**

L'objectif de ce module est de documenter les informations qui remontent lors des projets pour les mettre en accès libre. Comme on a vu dans l'atelier, ça pose quelques questions par rapport au lobbying et le spam donc l'idée serait que seuls les besoins collectés en personne puissent être chargés sur le site.

+ Téléchargement d'un fichier .csv
+ Liaison de ce fichier à un projet

**2. Etablir des rapports**

Aggréger les données pour obtenir une vision plus lisible des besoins est probablement le défi technique le plus difficile à relever.
Plutôt que de parler d'IA et de traitement sémantique ultraperfectionné, il vaut mieux à mon avis fournir pour l'instant des outils d'aide au traitement pour les humains qui vont bosser sur cette étape. Typiquement, ceux qui bossent sur la gestion des réponses du grand débat doivent avoir des idées :-p

+ Aide au regroupement des réponses similaires
+ Outil d'aide à la manipulation du texte
+ Outil collaboratif de traitement de données
+ Visualisation, catégorisation des réponses


**3. Cartographier les besoins**

L'objectif de ce module est de lier les besoins à un territoire donné. La question de la précision se pose. L'essentiel ( à mon humble avis mais ça se discute) est que les données restent anonymes. Du coup mettre l'adresse irait contre cette idée. Le nom de la rue semble suffisant.
Du coup, cela veut dire que la méthode doit être adaptée pour demander aussi le nom de la rue et la commune dans laquelle ils habitent.
Et une fois l'étape 2 terminée, on peut nourrir la carte.
Un autre défi est qui emerge est le contexte, pas uniquement le territoire. Par exemple, les questions pour la collecte, tout en restant larges, peuvent être adaptées à un domaine, une activité, un groupe particulier (voir le questionnaire pour faire remonter les besoins chez les acteurs de la transition, pas localisé mais lié à un contexte)

+ Carte des réponses
+ Système de filtre par catégorie, mot clé, projet


**4. Organiser des Ateliers participatifs**

Ce module est finalement juste un outil de gestion d'événements et de communication. Pour faciliter la tâche des organisateurs, on peut créer un modèle d'événement Solucracy à copier en adaptant le thème du forum, la date, le lieu.

+ Création d'événement
+ Communication des événements
+ Export vers calendriers
+ Création des fichiers types (dossier du participant, affiches,etc..)
+ Liste modèle de choses à faire pour préparer l'événement que l'on peut ajuster en fonction du contexte.
+ Tout ce qu'il y a déjà sur les outils de gestion d'événement ( inscription, contacter les participants, tout ça, tout ça)

**5. Créer des projets** 

Pour ce module, l'idée est de fournir un support de communication pour les groupes projets.

+ Espace de discussion
+ Description du projet
+ Outils d'aide à la prise de décision en groupes
+ Organisation de réunion
+ Catégorisation/thématique
+ Gestion des participants
+ Liste de tâches/ étapes
+ Gestion ressources/budget


**6. Mettre en réseau les projets**

Les participants à un projet sur une thématique donnée à un niveau local doivent pouvoir discuter avec d'autres dans la même approche sur un territoire plus grand. On est en mode coopération, pas compétition donc les échanges d'expériences sont nécessaires pour avancer ensemble. Il y a là une problématique très intéressante mais qui me retourne le cerveau à chaque fois que j'y réfléchis donc pardon si la suite est un peu longue.

L'approche ici serait de trouver un système de documentation qui part du local pour remonter vers le général. Comment structurer l'information pour qu'une personne qui mette en place un jardin partagé dans le Var partage son expérience sur les jardins partagés à un niveau, et son expérience sur son projet particulier à un autre niveau ? Et ensuite, on peut rajouter jardin partagé en milieu urbain, ou jardin partagé en milieu rural, sur le toit d'un immeuble, dans une vallée, en haut d'une montagne, dans la jungle, etc...
Est-ce qu'il est possible de donner un contexte à chaque phrase ?
Si je parle du financement donc ça s'applique à tous les jardins partagés ou tous les projets ? 

Si ce sujet vous intéresse, n'hésitez pas à me contacter pour brainstormer des trucs. C'est à mon avis lié au module 2 cité plus haut.

En attendant, faisons simple :

+ Un forum par catégorie de projet (par exemple : Jardins partagés, Tiers Lieu, etc...)
+ Un forum sur la méthode de gestion de projet (Recrutement de bénévoles, animations de réunion, financement, etc...)
+ Des liens/newsletter/agendas partagés entre catégories de projets.

**7. Collaborer sur la méthode et documenter les expériences**

Cette partie est prise en charge par la refonte du site sur lequel Sabrina Vuillemenot est en train de travailler avec les fonctionnalités suivantes :
+ Blogging + commentaires ( parce que là, le blog est quand même trop unilatéral )
+ Newsletter
+ Création d'un livre collaboratif virtuel ( mais imprimable si vous aimez vraiment pas les arbres :-) )

Le tout en CC by SA , donc au moment de contribuer, il faut quand même faire approuver son compte pour éviter du spam.

L'intention est que le mot Solucracy soit connu et permette de créer un truc autour, pas pour la gloire mais plutôt dans l'esprit que dire Lego est plus simple que de dire "petits bouts de plastiques qui s'emboitent pour que les enfants puissent jouer avec et détruire les pieds des adultes qui se promènent en chaussettes".

