---
layout: post
title: "Comment puis-je aider ?"
date:   2018-06-26
lang: fr
ref: how-help
author: Yannick
categories: solucracy
---
Il y a plusieurs choses que vous pouvez faire pour nous aider à faire avancer Solucracy :

- Partager <https://solucracy.com> avec vos amis
- Aimer et partager la page [Facebook][fb]
- En parler autour de vous quand ça manque de sujets de conversation
- Vous connecter sur le site et voter/commenter les problèmes, ajouter de nouveaux problèmes, participer !
- Partager les problèmes qui vous tiennent à coeur sur les réseaux sociaux et susciter le débat
- Vous pensez pouvoir utiliser cet outil pour un projet perso ou professionnel, contactez-nous, peut-être qu’on peut faire évoluer le site pour répondre à vos besoins de manière plus précise.
- Nous faire part de vos suggestions/ idées pour améliorer le site, ou nous dire pourquoi vous pensez que c’est une mauvaise idée, pourquoi vous ne voulez pas participer, critiquer l’interface
- Courir tout nu dans la rue en hurlant Solucracy.com dans un mégaphone ( pour ceux qui ont plutôt un tempérament d’activiste d’extérieur )

[fb]: https://www.facebook.com/solucracy
