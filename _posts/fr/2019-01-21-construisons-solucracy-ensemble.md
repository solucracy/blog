---
layout: post
title: "Construisons Solucracy ensemble !"
date: 2019-01-21
lang: fr
ref: construisons-solucracy-ensemble
author: Yannick
categories: solucracy
---

Le projet pilote est presque terminé et la méthode est bientôt complètement documentée.
Elle va pouvoir prendre son envol pour aller accompagner d'autres projets Solucracy un peu partout en France et s'enrichir de ces expériences.
Pour que ce soit le plus simple possible à mettre en place et que ces projets profitent au plus grand nombre, il est important que l'application soit adaptée et au service des citoyens qui se lanceront dans ces projets.

Pour être sûrs que ce soit le cas, nous vous proposons de définir avec nous le cahier des charges !
Le 2 mars de 13h à 18h30 (après c'est l'apéro ☺ ) à [Ideavox](http://www.ideavox.org/) , venez décider avec nous quelles fonctionnalités doit comprendre l'application Solucracy pour être bénéfique à tous.

Vous pouvez vous inscrire en cliquant [ici](https://www.eventbrite.fr/e/inscription-solucracy-un-tiers-lieu-pour-la-feuille-de-route-55146059348) 

Cet événement se terminera par un buffet canadien et une surprise ! ☺

Suivant ce qui émergera de cet atelier, il est tout à fait possible que Solucracy utilise un amalgame d'outils libres déjà existants.
Après tout quoi de mieux pour assurer sa [compostabilité](https://ressources.osons.cc/?PetitPrecisDeCompostabiliteDesprojets4) ? ☺

En plus de cet événement, à partir du 30 Janvier, s'ouvre un espace virtuel régulier pour parler de Solucracy tous les mercredis de 18h à 19h.

Le concept est simple : A 18h le mercredi, vous cliquez sur [ce lien](https://zoom.us/j/3497862775) et vous rejoignez la conversation.

Nous pourrons alors discuter de Solucracy, de son futur, nous conseiller les uns les autres pour lancer divers projets, et améliorer tout ça ensemble.

Vous pouvez considérer cela comme un salon virtuel pour discuter toutes les semaines avec les contributeurs du projet en live ☺

Et si vous préférez parler du projet en dehors de ça, alors vous pouvez nous rejoindre sur [le chat des crapauds fous](https://coa.crapaud-fou.org/channel/solucracy) !

Au plaisir de vous y retrouver !
