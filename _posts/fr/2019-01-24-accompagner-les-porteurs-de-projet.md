---
layout: post
title: "Accompagner les porteurs de projet"
date: 2019-01-24
lang: fr
ref: accompagner-les-porteurs-de-projet
author: Yannick
categories: solucracy
tags: method
---

A l'issue des ateliers participatifs, vous obtiendrez donc une liste de projets, avec pour chacun une liste d'emails.

Il est important d'accompagner les porteurs de projet. Les initiatives sont encore toutes fraiches et il faut capitaliser sur l'énergie générée pendant l'atelier. Les citoyens engagés doivent sentir qu'ils ne sont pas seuls.

Au maximum 4 à 5 jours après l'atelier, il est utile d'envoyer à chaque groupe un email récapitulatif avec quelques pistes pour avancer ensemble vers leurs objectifs.

Voici un exemple d'email que vous pouvez envoyer :

> Bonjour !
> 
> Comme promis, voici donc un petit email avec la récap des infos sur le projet pour lequel vous vous êtes inscrit lors de l'atelier Solucracy et quelques pistes qui peuvent être utiles.
> Pour faire un point dans 1 mois, voici un petit sondage pour décider de la date : **[vous pouvez utiliser le site framadate pour créer le sondage](https://framadate.org/)**
> 
> En attendant,  il est important que vous puissiez vous réunir au moins une fois pour discuter le projet et les prochaines actions. Voici quelques questions que vous pouvez vous poser sur le projet pour aider à le clarifier et vous assurer que toute l'équipe est alignée sur la même vision :
> 
>     *  La raison d’être du projet: pourquoi faire ce projet et à quoi cela va servir?
>     *  Description du projet aujourd’hui: de quoi s’agit-il?
>     *  Description du projet une fois terminé: a quoi cela va t’il ressembler?
>     *  Les besoins qu’il rempli
>     *  Les bénéfices à court, moyen et long terme
>     *  Les membres actuels, les compétences existantes, les compétences manquantes
>     *  Les étape clés pour que le projet se réalise
>     *  Ressources internes et externes disponibles et à trouver
> 
> 
> Recruter des gens :
> 
> L'équipe peut encore grandir, il peut être utile de réfléchir à une ou deux phrases pour résumer le projet et en parler autour de vous, les poster sur les réseaux sociaux ou demander à lancer un appel sur le Grêlon.
> 
> Mais surtout :
> 
> N'oubliez pas de faire la fête, de célébrer, de vous faire plaisir et de vous amuser ! C'est le principal ! ☺
> 
> 
> Liens utiles :
> 
> **Insérez ici quelques liens pertinents par rapport au projet mentionné**
> 
> Voici les infos de votre projet :
> 
> **Insérez ici les infos collectées durant l'atelier** 
