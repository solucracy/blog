---
layout: post
title: "2 étapes majeures pour Solucracy !"
date: 2018-12-10
lang: fr
ref: 2-etapes-majeures-pour-solucracy
author: Yannick
categories: solucracy
---
La semaine dernière, le projet Solucracy a franchi 2 paliers très importants dans son développement et c'est un plaisir de les partager ici avec vous :

### Le code est libre !!! Libre !!!!

Grâce à l'aide de [Mose](http://mose.com/index-fr.html) et du [collectif des crapauds fous ,](https://crapaud-fou.org/) le code a maintenant été libéré et vous pouvez le retrouver [ici sur Gitlab](https://gitlab.com/solucracy/solucracy) en licence GNU-GPL.

Il reste certes pas mal de nettoyage à faire après 4 ans de développement tout seul dans mon coin mais il est maintenant en train de s'aérer tranquillement :-) . Mose a mis en place tout ce qu'il faut pour pouvoir installer chez soi gratuitement une instance de Solucracy avec Docker. Si vous avez des questions, si vous souhaitez contribuer, n'hésitez pas à nous rejoindre sur le [Rocket Chat des Crapauds fous](https://coa.crapaud-fou.org/) canal #Solucracy.

Le collectif des Crapauds Fous pourra probablement aussi vous intéresser si vous êtes à la recherche d'une communauté motivée pour faciliter la transition ou avoir plein de conversations passionnantes sur des tonnes de sujets. Venez plonger dans la Mare, vous ne le regretterez pas ! :-)

### Le projet pilote a commencé !! \o/

Les 700 habitants de [Léaz (01200)](https://www.leaz.fr/-Francais-.html), commune regroupant Léaz, Grésin et Longeray vont être les premiers à tester la méthode Solucracy !

Cette semaine, du 10 au 15 décembre, Nicolas, Paloma, Stephan et moi allons passer dans chacun des foyers du village pour recueillir les besoins, envies, problèmes des habitants.

Nous consoliderons ensuite ces données pour organiser un atelier participatif le 12 Janvier dans la salle des fêtes où chacun aura la possibilité de s'exprimer et de proposer des projets pour que la vie dans le village soit encore plus agréable.

Nous avons délibérément choisi un cadre restreint pour tester et affiner la méthode. Une fois le projet terminé, cette méthode sera documentée et mise en creative commons sur un wiki pour que tout le monde puisse se l'approprier, la reproduire et la bonifier.

L'application sera ensuite mise à jour en fonction des enseignements tirés du projet pour avoir un outil numérique cohérent en soutien d'une méthode qui fonctionne déjà dans la vie réelle ( dans la vie réelle, ça veut dire IRL en fait :-p )

Solucracy grandit, se libère et fait ses propres expériences ! :-)
